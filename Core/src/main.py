import argparse

parser = argparse.ArgumentParser()
parser.add_argument("grid_size", type=int)
parser.add_argument("time_steps", type=int)
args = parser.parse_args()
