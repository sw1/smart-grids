__author__ = 'Adam Świeżowski'


class Agent:
    """Agent is C_n(r) from article second point, time is same for all agents in one round. Agent can be called as
    player and state is amount of investment"""

    def __init__(self):
        self.state = 0     # state = [0, 1]


rules = []


def init_lattice(size):
    return [[Agent()] * size] * size


default_lattice = init_lattice(5)

time = 1
state_history = {}     # should have format of time:lattice